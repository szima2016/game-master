import { Test } from '@nestjs/testing';
import { AppModule } from './../src/app.module';
import { INestApplication } from '@nestjs/common';
import * as mongoose from 'mongoose';
import MongoMemoryServer from 'mongodb-memory-server';
import { Model, Document } from 'mongoose';

// there must be argument with jest --detectOpenHandles in order to correctly
// end test because there is a problem with async operation writing to database
// https://github.com/facebook/jest/issues/7287


describe('AppController (e2e)', () => {

  let app: INestApplication;
  let mongod: MongoMemoryServer;
  let Call: Model<Document, {}>;

  beforeAll(async () => {

    mongod = new MongoMemoryServer();

    const mongoUri = await mongod.getConnectionString();

    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider('DbConnectionToken')
      .useFactory({
        factory: async (): Promise<typeof mongoose> => await mongoose.connect(
          mongoUri,
          { useNewUrlParser: true }
        ),
      })
      .compile();

    Call = moduleFixture.get('CallModelToken')
    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('...', () => {
    it("...", async () => {

      const testingData: any[] = [
        new Call({ called_at: new Date("2019-01-05T11:48:21.679Z") }),
        new Call({ called_at: new Date("2019-01-05T11:49:44.747Z") }),
        new Call({ called_at: new Date("2019-01-06T17:48:46.679Z") }),
        new Call({ called_at: new Date("2019-01-07T13:58:21.679Z") }),
        new Call({ called_at: new Date("2019-01-07T12:24:21.679Z") }),
        new Call({ called_at: new Date("2019-01-09T19:16:21.679Z") }),
      ];
      
      return Call.insertMany(testingData).then((data: any[]) => {
        data.forEach((call) => {
          expect(call).toHaveProperty('called_at', expect.any(Date));
        });
      })

      // how many callbacks it has
      // const Call = mongoose.model('User', new mongoose.Schema());
      // const cnt = await User.count();
      // expect(cnt).to.equal(0);
    });
  });

  afterAll( async () => {
    await mongoose.disconnect();
    await mongod.stop()
    await app.close();
  });

});
