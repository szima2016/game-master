import { Injectable, Inject } from "@nestjs/common";
import { Model } from "mongoose";
import { Call } from "./interfaces/call.interface";
import { CreateCallTimestampDto } from "./dto/create-call-timestamp.dto";

enum DaysOfWeek {
  sunday = 1,
  monday,
  tuesday,
  wednesday,
  thursday,
  friday,
  saturday,
}

@Injectable()
export class CallService {

  constructor(@Inject('CallModelToken') private readonly callModel: Model<Call>) { }

  async saveTimestamp(createCallTimestampDto: CreateCallTimestampDto): Promise<Call> {
    const createdTimestamp = new this.callModel(createCallTimestampDto);
    return await createdTimestamp.save();
  }

  // Considering putting query as return time of some QueryBuilder
  async findAll(dayOfWeek: number = DaysOfWeek.monday): Promise<Call[]> {
    return await this.callModel
      .find({
        $expr: {
          $eq: [
            {
              $dayOfWeek: "$called_at"
            },
            dayOfWeek
          ]
        }
      })
      .exec();
  }

}