import { Controller, Get, UseGuards } from "@nestjs/common";
import { CallService } from "./call.service";
import { AuthGuard } from "@nestjs/passport";

@Controller('calls')
export class CallController {

  constructor(private readonly callService: CallService) { }

  @Get('/')
  @UseGuards(AuthGuard())
  async getCalls() {
    try {
      return await this.callService.findAll();
    } catch (error) {
      throw new Error(error);
    }
  }}