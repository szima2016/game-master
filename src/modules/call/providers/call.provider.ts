import { Connection } from "mongoose";
import { CallSchema } from "../schemas/call.schema";

export const callPoviders = [
  {
    provide: 'CallModelToken',
    useFactory: (connection: Connection) => connection.model('Call', CallSchema),
    inject: ['DbConnectionToken'],
  },
];