export interface CreateCallTimestampDto {
  readonly called_at: Date;
}
