import * as mongoose from 'mongoose';

export const CallSchema = new mongoose.Schema({
    called_at: Date
});