import { Module } from "@nestjs/common";
import { DatabaseModule } from "../../database/database.module";
import { PassportModule } from "@nestjs/passport";
import { AuthModule } from "../../auth/auth.module";
import { CallService } from "./call.service";
import { callPoviders } from "./providers/call.provider";
import { CallController } from "./call.controller";

@Module({
  imports: [DatabaseModule, PassportModule, AuthModule],
  providers: [CallService, ...callPoviders],
  controllers: [CallController],
  exports: [CallService]
})
export class CallModule {}
