import { Document } from 'mongoose';

export interface Call extends Document {
    called_at: Date;
}