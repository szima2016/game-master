import { Test, TestingModule } from '@nestjs/testing';
import { GameService } from './game.service';
import { ICheapSharkDeal } from './interfaces/cheapSharkDeal.interface';
import { ICheapSharkGame } from './interfaces/cheapSharkGame.interface';

describe('GameService', () => {

  let service: GameService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GameService],
    }).compile();
    service = module.get<GameService>(GameService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });


  // Example of how would I write tests
  describe('buildGamesData()', () => {

    it('should return array of games in IGame[] format', async () => {

      // I was considering putting those constant into separate 
      // file for better modularity a readability of code
      const deals: ICheapSharkDeal[] = [{
        internalName: 'GRANDTHEFTAUTOV',
        title: 'Grand Theft Auto V',
        metacriticLink: '/game/pc/grand-theft-auto-v',
        dealID: 'RIpggaA%2BJrgBSDU3iSWZVm%2BiMQsr5XCwyDY3Do9B5CA%3D',
        storeID: '1',
        gameID: '113480',
        salePrice: '29.99',
        normalPrice: '29.99',
        isOnSale: '0',
        savings: '0.000000',
        metacriticScore: '96',
        steamRatingText: 'Mixed',
        steamRatingPercent: '69',
        steamRatingCount: '386507',
        steamAppID: '271590',
        releaseDate: 1428969600,
        lastChange: 1546540803,
        dealRating: '0.0',
        thumb:
          'https://steamcdn-a.akamaihd.net/steam/apps/271590/capsule_sm_120.jpg?t=1544815097'
      }];

      const game: ICheapSharkGame[] = [{
        "info": {
          "title": "Grand Theft Auto V",
          "steamAppID": "271590"
        },
        "cheapestPriceEver": {
          "price": "11.98",
          "date": 1546194557
        },
        "deals": [
          {
            "storeID": "23",
            "dealID": "RDUXtlLWnb8IXOklcLSnragp6UOdORzQU4cj9AlHH0M%3D",
            "price": "24.99",
            "retailPrice": "29.99",
            "savings": "16.672224"
          },
          {
            "storeID": "18",
            "dealID": "bnl%2FD3D2GcTf6GqFgNACMGtNpR1WDsOwmugoNIIrFxM%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          },
          {
            "storeID": "6",
            "dealID": "FKPtu3WHPwlSD7WnQyM%2FVI9QAN65SkL%2FZgw%2F2EEB3Eg%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          },
          {
            "storeID": "1",
            "dealID": "RIpggaA%2BJrgBSDU3iSWZVm%2BiMQsr5XCwyDY3Do9B5CA%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          },
          {
            "storeID": "5",
            "dealID": "cEQ03bF8bEX4apU7UboPAKNE%2BzAjw2DNOKI3suOVf74%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          },
          {
            "storeID": "11",
            "dealID": "I6WPVDX2kVl7fp2vBA9B%2FJLLXMNZW2bn1z5rb2rMMgE%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          },
          {
            "storeID": "3",
            "dealID": "76e3RkJ6NaddNvxxRcBUOsAxBVkvMKLfIsX41ldC770%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          },
          {
            "storeID": "2",
            "dealID": "xnHYM1DDbzRxC86rthIOK5zUqqMZefkw%2BPvbbXfXQl0%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          },
          {
            "storeID": "15",
            "dealID": "OcvPvMv09rjHEQSGH9u4o5XB4aUE2lozaSj1Rx0y40Q%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          },
          {
            "storeID": "21",
            "dealID": "ttlV2zwlUPa1f%2BmwTyubgZwziQMRHdoeqqSfO2dlsVE%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          },
          {
            "storeID": "24",
            "dealID": "50q%2Bhqaz7lLOEkSDtzmtRBBy7PjinfoRam4TMfOVms0%3D",
            "price": "29.99",
            "retailPrice": "29.99",
            "savings": "0.000000"
          }
        ]
      }];

      const result = service.buildGamesData(deals, game);

      expect(result).toEqual([{
        cheapestPrice: 11.98,
        name: "Grand Theft Auto V",
        releaseDate: new Date("2015-04-14T00:00:00.000Z"),
        salePrice: 29.99
      }]);
    })

  })
});
