import { Test } from '@nestjs/testing';
import { GameController } from './game.controller';
import { GameService } from './game.service';

describe('GameController', () => {
  let gameController: GameController;
  let gameService: GameService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [GameController],
      providers: [GameService]
    }).compile();

    gameService = module.get<GameService>(GameService);
    gameController = module.get<GameController>(GameController);
  });

  it('should be defined', () => {
    expect(GameController).toBeDefined();
  });

  describe('getGameInfo()', () => {
    it('should return an array of games', async () => {
      const result = [{
        cheapestPrice: 11.98,
        name: "Grand Theft Auto V",
        releaseDate: new Date("2015-04-14T00:00:00.000Z"),
        salePrice: 29.99
      }];
      jest.spyOn(gameService, 'getGames').mockImplementation(() => result);

      expect(await gameController.getGameInfo()).toBe(result);
    });
  });
});