import { Controller, Get, Req } from '@nestjs/common';
import { GameService } from './game.service';

@Controller('games')
export class GameController {

  constructor(private readonly gameService: GameService) { }

  // I'm able to specify Query params but I left it for simplicity
  @Get('/')
  async getGameInfo() {
    try {
      return await this.gameService.getGames();
    } catch (error) {
      throw new Error(error);
    }
  }
}
