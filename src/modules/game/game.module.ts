import { Module } from '@nestjs/common';
import { GameService } from './game.service';
import { GameController } from './game.controller';
import { DatabaseModule } from './../../database/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [GameService],
  controllers: [GameController],
})
export class GameModule {}
