import { Injectable } from '@nestjs/common';
import { IGame } from './interfaces/game.interface';
import Axios, { AxiosResponse } from 'axios';
import { ICheapSharkDeal } from './interfaces/cheapSharkDeal.interface';
import { ICheapSharkGame } from './interfaces/cheapSharkGame.interface';

@Injectable()
export class GameService {

  // URL as readonly properties due to simplify task (not proper way)
  // I am able to create parametrized routes as specified in Cheapshark API in more generic way
  // but I tried to strictly follow objective a focus only on API url from forked repo
  // Also I would put core URL "http://www.cheapshark.com/api/1.0" into configuration file or specity it
  // as standalone constant if I would be implementing more endpoints.
  private readonly dealsUrl: string = 'http://www.cheapshark.com/api/1.0/deals?storeID=1&desc=0&title=grand%20theft%20auto&pageSize=20';
  private readonly gameLookupUrl: string = 'http://www.cheapshark.com/api/1.0/games?id=';

  public async getGames(): Promise<IGame[]> {
    const deals = (await this.fetchGamesDeals()).data;
    const games = await this.getGamesInfo(this.getGamesIDs(deals));

    const resultGameData: IGame[] = this.buildGamesData(deals, games);

    return resultGameData;
  }

  public async fetchGamesDeals(): Promise<AxiosResponse> {
    const response: AxiosResponse<ICheapSharkDeal> = await Axios.get(this.dealsUrl);

    if (response.status === 200) {
      return response;
    }

    throw new Error('Error fetching games deals');
  }

  private getGamesIDs(deals: ICheapSharkDeal[]): number[] {
    return deals.map((deal: ICheapSharkDeal) => {
      return parseInt(deal.gameID);
    })
  }

  public async getGamesInfo(gameIDs: number[]): Promise<ICheapSharkGame[]> {
    return Axios.all([...gameIDs.map(id => this.gameLookup(id))])
      .then(Axios.spread(function (...responses: AxiosResponse[]) {
        return responses.map((response: AxiosResponse) => response.data as ICheapSharkGame);
      }))
      .catch(() => {
        throw new Error('Error getting games info');
      });
  }

  public gameLookup(id: number): Promise<AxiosResponse> {
    return Axios.get(this.gameLookupUrl + id.toString());
  }

  public buildGamesData(deals: ICheapSharkDeal[], games: ICheapSharkGame[]): IGame[] {
    return deals.map((deal: ICheapSharkDeal) => {
      return {
        name: deal.title,
        salePrice: parseFloat(deal.salePrice),
        cheapestPrice: this.getGameCheapestPrice(deal.title, games),
        releaseDate: this.getReleaseDateInCorrectForm(deal.releaseDate)
      }
    })
  }

  // moment.js could be a better option maybe
  private getReleaseDateInCorrectForm(time: number): Date {

    if (time === 0) {
      return null;
    }

    // Converting to milliseconds
    // Reason: Epoch is usually expressed in seconds, but Javascript uses Milliseconds
    if (time < 10000000000) 
      time *= 1000;

    const date = new Date();
    date.setTime(time);

    return date;
  }

  private getGameCheapestPrice(title: string, games: ICheapSharkGame[]): number {
    const game = games.find((game: ICheapSharkGame) => game.info.title === title);

    if (game) {
      return parseFloat(game.cheapestPriceEver.price);
    }

    throw new Error('Cannot find game among received games (should never happend)')
  }
}
