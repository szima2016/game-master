interface CheapSharkGameInfo {
    readonly title: string;
    readonly steamAppID: string;
}

interface CheapSharkCheapestPriceEver {
    readonly price: string,
    readonly date: number;
}

interface CheapSharkGameDeal {
    readonly storeID: string;
    readonly dealID: string;
    readonly price: string;
    readonly retailPrice: string;
    readonly savings: string;
}

export interface ICheapSharkGame {
    info: CheapSharkGameInfo;
    cheapestPriceEver: CheapSharkCheapestPriceEver;
    deals: CheapSharkGameDeal[];
}