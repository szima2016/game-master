import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as Path from 'path';
import * as appRoot from 'app-root-path';

interface ConfigObject {
    [key: string]: string
}

export class ConfigService {
    private readonly envConfig: ConfigObject;
    private readonly configFileLocation: string = `${Path.resolve(appRoot.path, 'development.env')}`;

    constructor() {
        this.envConfig = dotenv.parse(fs.readFileSync(this.configFileLocation))
    }

    get(key: string): string {
        return this.envConfig[key];
    }
}