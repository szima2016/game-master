import { Injectable, NestMiddleware, MiddlewareFunction } from '@nestjs/common';
import { CallService } from '../../modules/call/call.service';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {

    constructor(private readonly callService: CallService) { }

    async resolve(): Promise<MiddlewareFunction> {

        return async (request, response, next) => {
            await this.callService.saveTimestamp({
                called_at: this.calculateTimeForCurrentTimezone(2)
            })

            next();
        };
    }

    private calculateTimeForCurrentTimezone(offset: number): Date {

        // Create "Date" object for current location
        const date = new Date();

        // Get UTC time in milliseconds
        const utc = date.getTime() + (date.getTimezoneOffset() * 60000);

        // Create new Date object using supplied offset
        const resultDate = new Date(utc + (3600000 * offset));

        return resultDate;
    }
}