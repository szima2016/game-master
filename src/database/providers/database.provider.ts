import { ConfigService } from '../../config/config.service';
import * as mongoose from 'mongoose';

export const DbConnectionFactory = {
    provide: 'DbConnectionToken',
    useFactory: async (configService: ConfigService): Promise<typeof mongoose> =>
            await mongoose.connect(
                    `mongodb://${configService.get('DATABASE_HOST')}:${configService.get('DATABASE_PORT')}/${configService.get('DATABASE_NAME')}`,
                    { useNewUrlParser: true }
                ),
    inject: [ConfigService],
  };