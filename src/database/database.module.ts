import { Module } from '@nestjs/common';
import { DbConnectionFactory } from './providers/database.provider';

@Module({
    providers: [DbConnectionFactory],
    exports: ['DbConnectionToken'],
})
export class DatabaseModule {}