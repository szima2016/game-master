import { Module, MiddlewareConsumer } from '@nestjs/common';
import { GameModule } from './modules/game/game.module';
import { CallModule } from './modules/call/call.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { AuthService } from './auth/auth.service';
import { ConfigService } from './config/config.service';
import { LoggerMiddleware } from './common/middlewares/logger.middleware';

@Module({
  imports: [GameModule, CallModule, AuthModule, ConfigModule],
  controllers: [],
  providers: [AuthService, ConfigService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes('games');
  }
}
