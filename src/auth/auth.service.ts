import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload.interface';

export interface TokenObject {
    readonly accessToken: string;
}

@Injectable()
export class AuthService {
    constructor(private readonly jwtService: JwtService) { }

    async createToken(): Promise<TokenObject> {
        const user: JwtPayload = { email: 'test@email.com' };
        const accessToken = this.jwtService.sign(user);
        return { accessToken };
    }

    // User validation logic should be implemented in here
    async validateUser(payload: JwtPayload): Promise<any> {
        return {};
    }
}
